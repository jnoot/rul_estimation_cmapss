
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import datetime
import itertools
import matplotlib
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, mean_squared_error, confusion_matrix, multilabel_confusion_matrix
import time
import random
pd.options.mode.chained_assignment = None  # type: ignore # default='warn'
from torch.nn.functional import normalize
from sklearn import preprocessing
from torch.autograd import Variable


############################################### Architecture LSTM OR
class LSTM_OR(nn.Module):
    def __init__(self, num_classes, input_size, hidden_size, num_layers, device):
        super(LSTM_OR, self).__init__()
        self.num_classes = num_classes # number of classes taille du vecteur dans notre cas
        self.num_layers = num_layers # number of layers
        self.input_size = input_size # input size
        self.hidden_size = hidden_size # hidden state
        self.device = device

        self.lstm1 = nn.LSTM(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True, dropout = 0.2)
        self.fc1 = nn.Linear(hidden_size, 32)  # fully connected layer
        self.fc2 = nn.Linear(32, num_classes) # fully connected last layer
        #fonction d'activation
        self.relu = nn.ReLU()
        #sigmoid dans notre cas
        self.sigmoid = nn.Sigmoid()
        
    def forward(self,x):

        device = self.device
        x = x.unsqueeze(0)
        n_samples = x.size(0)

        ht_1 = Variable(torch.zeros(self.num_layers, n_samples, self.hidden_size)).to(device) # hidden state
        ct_1 = Variable(torch.zeros(self.num_layers, n_samples, self.hidden_size)).to(device) # internal state

        ht_1, ct_1 = self.lstm1(x, (ht_1, ct_1))
        ht_1 = self.relu(ht_1)
        out = self.fc1(ht_1) # Final Output of the LSTMS
        out = self.relu(out)
        out = self.fc2(out)
        out = self.sigmoid(out) #sigmoid
        return out